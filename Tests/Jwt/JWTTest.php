<?php

namespace Tests\Jwt;
use Ds\Jwt\JWT;
use Ds\Jwt\Parser;

/**
 * Class JWTTest
 * @package Tests\Ds\Jwt
 */
class JWTTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var Parser
     */
    public $parser;
    /**
     * @var JWT
     */
    public $token;

    public function setUp() : void {
        $this->parser = $this->getMockBuilder('\Ds\Jwt\Parser')->getMock();
        $this->token = new JWT($this->parser);
    }

    public function testVerify(){

        $signature = $this->getMockBuilder('\Ds\Jwt\SignatureInterface')->getMock();
        $tokenVerify = $this->token->withSignature('RS256', $signature);

        $signature->expects($this->once())
            ->method('verify')
            ->with(
                $this->equalTo($this->parser)
            );

        $tokenVerify->verify();
    }

    public function testWithId(){
        $this->parser->expects($this->once())
            ->method('setClaim')
            ->with(
                $this->equalTo('jti'),
                $this->equalTo('id')
            );
        $this->token->withId('id');
    }

    public function testWithExpires(){
        $this->parser->expects($this->once())
            ->method('setClaim')
            ->with(
                $this->equalTo('exp'),
                $this->equalTo(1462548951)
            );
        $this->token->withExpires(1462548951);
    }

    public function testWithIssuer(){
        $this->parser->expects($this->once())
            ->method('setClaim')
            ->with(
                $this->equalTo('iss'),
                $this->equalTo(1462548951)
            );
        $this->token->withIssuer(1462548951);
    }

    public function testWithIssuedAt(){
        $this->parser->expects($this->once())
            ->method('setClaim')
            ->with(
                $this->equalTo('iat'),
                $this->equalTo(1462548951)
            );
        $this->token->withIssuedAt(1462548951);
    }

    public function testWithNotBefore(){
        $this->parser->expects($this->once())
            ->method('setClaim')
            ->with(
                $this->equalTo('nbf'),
                $this->equalTo(1462548951)
            );
        $this->token->withNotBefore(1462548951);
    }

    public function testWithAudience(){
        $this->parser->expects($this->once())
            ->method('setClaim')
            ->with(
                $this->equalTo('aud'),
                $this->equalTo('audience')
            );
        $this->token->withAudience('audience');
    }

    public function testWithSubject(){
        $this->parser->expects($this->once())
            ->method('setClaim')
            ->with(
                $this->equalTo('sub'),
                $this->equalTo(['subject'])
            );
        $this->token->withSubject(['subject']);
    }

    public function testWithClaim(){
        $this->parser->expects($this->once())
            ->method('setClaim')
            ->with(
                $this->equalTo('myclaim'),
                $this->equalTo('claim-value')
            );
        $this->token->withClaim('myclaim','claim-value');
    }
}
