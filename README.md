#Json Web Tokens

Json Web Tokens

```

//Create token object.
$token = new \Ds\Jwt\Token(
    new \Ds\Jwt\TokenParser()
);

//Create Signatures
$HsSignatute =  new \Ds\Jwt\Signature\HsSignature('my-secret')
$RsSignature =  new \Ds\Jwt\Signature\RsSignature(
                   file_get_contents(__DIR__ . '/key.pub'),
                   file_get_contents(__DIR__ . '/key.priv')
                );

$token = $token->withSignature('HS', $HsSignatute)
               ->withSignature('HS', $RsSignature)

//Define Claims.
$claims = [
  'jti' => 'my-id',
  'aud' => 'foo.bar',
  'nbf' => time()
];

try{
    $newToken = $token->createFromArray('HS256', $claims)
}catch(Exception $e){
    //algo not found.
}

//Return token string.
$newJwt = $newToken->create();

//Import rules into validator.
$newJwtRules = $validator->withRules($claims);

try{
    $token->import($newJwt, $newJwtRules)->verify();
}catch (\Exception $e){
    //token not valid.
}

$HS256Token = $token->withId('myId')
                     ->withIssuer('www.domain.com')
                     ->withAudience('foo.bar')
                     ->withExpires(time())
                     ->withIssuedAt(time())
                     ->withNotBefore(time()-600)
                     ->withSubject(['subject'])
                     ->withClaim('custom', 'my claim data')
                     ->withClaim('custom2', ['my claim data'])
                     ->useSignature('RS256')
                     ->create();

$validator = new \Ds\Jwt\Validator();

$validator->setId('myId')
            ->setIssuer('www.domain.com')
            ->setAudience('foo.bar')
            ->setExpires(time())
            ->setIssuedAt(time())
            ->setNotBefore(time()-600)
            ->setSubject(['subject'])
            ->setClaim('custom','my claim data')
            ->setClaim('custom2', ['my claim data']);

try{
    $result = $token->import($HS256Token, $validator)->verify(); //finds correct signature
    var_dump($result);
}catch (\Exception $e){
    echo $e->getMessage();
}

//

```


